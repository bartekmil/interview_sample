import { render, screen } from '@testing-library/react';
import { CandidateProvider } from 'context/CandidatesContext/CandidateProvider';
import * as React from 'react';

import { useCandidate } from '../src/context/CandidatesContext/useCandidate';
import { CandidatesRaport } from '../src/views/DashboardApp/CandidatesRaport/CandidatesRaport';

const mockUseCandidate = useCandidate as jest.Mock;
const mockData = {
  data: [
    {
      id: 1,
      name: 'andrzej',
      position: 'policeman',
      date: '2023-10-27',
      shortDescription: 'short description',
      longDescription: 'long description',
      logo: 'logo',
      companyName: 'company name',
    },
    {
      name: 'ja',
      position: 'ja',
      shortDescription: 'ja',
      date: '2023-10-27',
      longDescription: 'on',
      logo: 'logo',
      companyName: 'ja',
      id: 2,
    },
  ],

  isLoading: false,
  error: null,
};
jest.mock('../src/context/CandidatesContext/useCandidate');
test('renders candidates card with right number of candidates as mocked', () => {
  mockUseCandidate.mockReturnValue(mockData);
  render(
    <CandidateProvider>
      <CandidatesRaport />
    </CandidateProvider>,
  );
  expect(screen.getByText('Candidates')).toBeTruthy();
  expect(screen.getByText(mockData.data.length)).toBeTruthy();
});
test('check loading state', () => {
  mockUseCandidate.mockReturnValue({ ...mockData, isLoading: true });
  render(
    <CandidateProvider>
      <CandidatesRaport />
    </CandidateProvider>,
  );
  expect(screen.getByRole('progressbar')).toBeTruthy();
});
test('check error state', () => {
  mockUseCandidate.mockReturnValue({ ...mockData, error: 'error' });
  render(
    <CandidateProvider>
      <CandidatesRaport />
    </CandidateProvider>,
  );
  expect(screen.getByText('Something wrong')).toBeTruthy();
});
