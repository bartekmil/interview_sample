import { render, screen } from '@testing-library/react';
import { DashboardSidebar } from 'layouts/Dashboard/DashboardSidebar/DashboardSidebar';
import { navLinks } from 'layouts/Dashboard/DashboardSidebar/NavLinks';
import React from 'react';
import { MemoryRouter } from 'react-router';

const mockUseLocationValue = {
  pathname: '/testroute',
  search: '',
  hash: '',
  state: null,
};
jest.mock('react-router-dom', () => {
  return {
    ...jest.requireActual('react-router-dom'),
    useLocation: jest.fn().mockImplementation(() => {
      return mockUseLocationValue;
    }),
  };
});
afterEach(() => {
  jest.clearAllMocks();
});

test('renders candidates card with right number of candidates as mocked', () => {
  mockUseLocationValue.pathname = navLinks[0].path;

  render(
    <MemoryRouter>
      <DashboardSidebar />
    </MemoryRouter>,
  );
  {
    navLinks.map((navLink) => {
      expect(screen.getByText(navLink.title)).toBeTruthy();
    });
  }
});
