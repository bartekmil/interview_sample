import { render, screen } from '@testing-library/react';
import React from 'react';

import { JobProvider } from '../src/context/JobContext/JobProvider';
import { useJob } from '../src/context/JobContext/useJob';
import { GeneralRaport } from '../src/views/DashboardApp/GeneralRaport/GeneralRaport';

jest.mock('../src/context/JobContext/useJob');

const mockUseJob = useJob as jest.Mock;

const mockData = {
  data: [
    {
      id: 1,
      title: 'Frontend Developer',
      date: '2023-10-27',
      shortDescription: 'short description',
      longDescription: 'long description',
      logo: 'logo',
      companyName: 'IT SP.zoo',
    },
  ],

  isLoading: false,
  error: null,
};
test('renders candidates card with right number of candidates as mocked', () => {
  mockUseJob.mockReturnValue(mockData);
  render(
    <JobProvider>
      <GeneralRaport />
    </JobProvider>,
  );
  expect(screen.getByText('Open positions')).toBeTruthy();
  expect(screen.getByText(mockData.data.length)).toBeTruthy();
});
test('check loading state', () => {
  mockUseJob.mockReturnValue({ ...mockData, isLoading: true, data: null });
  render(
    <JobProvider>
      <GeneralRaport />
    </JobProvider>,
  );
  expect(screen.getByRole('progressbar')).toBeTruthy();
});
test('check error state', () => {
  mockUseJob.mockReturnValue({ ...mockData, error: 'error' });
  render(
    <JobProvider>
      <GeneralRaport />
    </JobProvider>,
  );
  expect(screen.getByText('Something wrong')).toBeTruthy();
});
