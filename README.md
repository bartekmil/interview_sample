

# Getting Started 

Interview project for interview company. It was designed for help users to manage their meetings, interview process and collect all informations about open positions for job and candidates.

## Available Scripts

To setup and run application:

-  `npm install`
-  `npm start`

To run unit tests:

- `npm test`


## Stack

- React
- Typescript
- Redux-toolkit
- Material UI 
- React-testing-library
- Jest
- Json-server 
- react-hook-form
- eslint
- prettier
