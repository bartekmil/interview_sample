import { createContext } from 'react';

import { CandidateContextTypes } from './CandidateProvider.types';

export const CandidateContext = createContext<
  CandidateContextTypes | undefined
>(undefined);
