import { useSafeContextFactory } from 'hooks/useSafeContextFactory/useSafeContextFactory';

import { CandidateContext } from './CandidateContext';

export const useCandidate = () =>
  useSafeContextFactory('Candidate', CandidateContext);
