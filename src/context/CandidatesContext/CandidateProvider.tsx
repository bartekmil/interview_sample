import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { useAxios } from 'hooks/useAxios';
import React, { useCallback, useMemo, useState } from 'react';

import { CandidateContext } from './CandidateContext';
import {
  CandidateProviderTypes,
  CandidateResult,
} from './CandidateProvider.types';

export const CandidateProvider = ({ children }: CandidateProviderTypes) => {
  const [selectedRows, setSelectedRows] = useState<number[]>([]);
  const [searchText, setSearchText] = useState('');

  const { removeElement, fetchData, data, isLoading, error } = useAxios<
    CandidateResult[]
  >({
    url: AppRoute.candidates,
    method: 'get',
  });

  const deleteCandidate = useCallback(
    (idsCandidate: number[]) =>
      Promise.all(
        idsCandidate.map((idCandidate) => {
          return removeElement(`${AppRoute.candidates}/${idCandidate}`);
        }),
      ).then(() => {
        setSelectedRows([]);
        fetchData();
      }),
    [fetchData, removeElement],
  );

  const value = useMemo(
    () => ({
      isLoading,
      error,
      data,
      selectedRows,
      setSelectedRows,
      deleteCandidate,
      searchText,
      setSearchText,
    }),
    [data, deleteCandidate, error, isLoading, searchText, selectedRows],
  );
  return (
    <CandidateContext.Provider value={value}>
      {children}
    </CandidateContext.Provider>
  );
};
