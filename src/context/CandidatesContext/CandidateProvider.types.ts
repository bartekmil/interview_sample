import { ReactNode } from 'react';
import { CandidateTypes } from 'views/Candidates/Candidate.types';

export interface CandidateResult extends CandidateTypes {
  id: number;
}
export interface CandidateContextTypes {
  data: CandidateResult[] | undefined;
  isLoading: boolean;
  error: string;
  selectedRows: number[];
  searchText: string;
  setSearchText: (searchText: string) => void;
  setSelectedRows: (
    idsRows: number[] | ((prevState: number[]) => number[]),
  ) => void;
  deleteCandidate: (idsCandidate: number[]) => Promise<void>;
}

export interface CandidateProviderTypes {
  children: ReactNode;
}
