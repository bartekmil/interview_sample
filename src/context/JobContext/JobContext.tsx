import { createContext } from 'react';

import { JobContextTypes } from './JobProvider.types';

export const JobContext = createContext<JobContextTypes | undefined>(undefined);
