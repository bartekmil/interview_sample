import { useSafeContextFactory } from 'hooks/useSafeContextFactory/useSafeContextFactory';

import { JobContext } from './JobContext';

export const useJob = () => useSafeContextFactory('Jobs', JobContext);
