import { ReactNode } from 'react';
import { JobTypes } from 'views/Job/Job.types';

export interface JobOfferResult extends JobTypes {
  id: number;
}
export interface JobContextTypes {
  data: JobOfferResult[] | undefined;
  isLoading: boolean;
  error: string;
  selectedRows: number[];
  searchText: string;
  setSearchText: (searchText: string) => void;
  setSelectedRows: React.Dispatch<React.SetStateAction<number[]>>;
  deleteJobOffer: (idsOffers: number[]) => Promise<void>;
}

export interface JobProviderTypes {
  children: ReactNode;
}
