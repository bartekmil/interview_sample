import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { useAxios } from 'hooks/useAxios';
import React, { useCallback, useMemo, useState } from 'react';

import { JobContext } from './JobContext';
import { JobOfferResult, JobProviderTypes } from './JobProvider.types';

export const JobProvider = ({ children }: JobProviderTypes) => {
  const [selectedRows, setSelectedRows] = useState<number[]>([]);
  const [searchText, setSearchText] = useState('');

  const { removeElement, fetchData, data, isLoading, error } = useAxios<
    JobOfferResult[]
  >({
    url: AppRoute.jobs,
    method: 'get',
  });

  const deleteJobOffer = useCallback(
    (idsOffers: number[]) =>
      Promise.all(
        idsOffers.map((idOffer) => {
          return removeElement(`${AppRoute.jobs}/${idOffer}`);
        }),
      ).then(() => {
        setSelectedRows([]);
        fetchData();
      }),
    [fetchData, removeElement],
  );

  const value = useMemo(
    () => ({
      isLoading,
      error,
      data,
      selectedRows,
      setSelectedRows,
      deleteJobOffer,
      searchText,
      setSearchText,
    }),
    [
      data,
      deleteJobOffer,
      error,
      isLoading,
      searchText,
      selectedRows,
      setSelectedRows,
    ],
  );
  return <JobContext.Provider value={value}>{children}</JobContext.Provider>;
};
