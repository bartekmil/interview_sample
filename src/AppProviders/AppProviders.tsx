import { CssBaseline } from '@mui/material';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { store } from 'redux/store';

import { AppProvidersProps } from './AppProviders.types';

export const AppProviders = ({ children }: AppProvidersProps) => {
  return (
    <>
      <CssBaseline />
      <Router>
        <Provider store={store}>{children}</Provider>
      </Router>
    </>
  );
};
