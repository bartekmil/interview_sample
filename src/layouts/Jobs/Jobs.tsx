import { JobProvider } from 'context/JobContext/JobProvider';
import { JobsNavbar } from 'views/Job/JobsNavbar/JobsNavbar';
import { JobsTable } from 'views/Job/JobsTable/JobsTable';

export const Jobs = () => {
  return (
    <JobProvider>
      <JobsNavbar />
      <JobsTable />
    </JobProvider>
  );
};
