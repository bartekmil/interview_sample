import { CandidateProvider } from 'context/CandidatesContext/CandidateProvider';
import { CandidateNavbar } from 'views/Candidates/CandidateNavbar/CandidateNavbar';
import { CandidateTable } from 'views/Candidates/CandidateTable/CandidateTable';

export const Candidates = () => {
  return (
    <CandidateProvider>
      <CandidateNavbar />
      <CandidateTable />
    </CandidateProvider>
  );
};
