import { Box } from '@mui/material';
import { CandidateProvider } from 'context/CandidatesContext/CandidateProvider';
import { JobProvider } from 'context/JobContext/JobProvider';
import { CandidatesRaport } from 'views/DashboardApp/CandidatesRaport/CandidatesRaport';
import { GeneralRaport } from 'views/DashboardApp/GeneralRaport/GeneralRaport';
import { OpenPositionsRaport } from 'views/DashboardApp/OpenPositionsRaport/OpenPositionsRaport';

import * as styles from './DashboardApp.styles';

export const DashboardApp = () => {
  return (
    <Box sx={styles.dashboardAppContainer}>
      <CandidateProvider>
        <CandidatesRaport />
      </CandidateProvider>
      <JobProvider>
        <OpenPositionsRaport />
        <GeneralRaport />
      </JobProvider>
    </Box>
  );
};
