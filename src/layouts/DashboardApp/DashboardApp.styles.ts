import { Styles } from 'styles/theme';

export const dashboardAppContainer: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'row',
  height: '20vh',
  width: '100%',
};
