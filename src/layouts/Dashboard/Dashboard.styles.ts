import { Styles } from 'styles/theme';

export const dashboardContainer: Styles = {
  padding: 2.5,
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
};
export const dashboardNavbarContainer: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
};
export const navIcon: Styles = { textSize: 1.5 };
export const sidebar: Styles = {
  width: '20vw',
};
export const outlet: Styles = {
  display: 'flex',
  width: '80vw',
  flexDirection: 'column',
};
export const dashboardContent: Styles = {
  display: 'flex',
  flex: '1 1 0%',
};
