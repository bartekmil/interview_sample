import { Box, Grid } from '@mui/material';
import { Outlet } from 'react-router-dom';

import * as styles from './Dashboard.styles';
import { DashboardNavbar } from './DashboardNavbar/DashboardNavbar';
import { DashboardSidebar } from './DashboardSidebar/DashboardSidebar';

export const Dashboard = () => {
  return (
    <Box sx={styles.dashboardContainer}>
      <DashboardNavbar />

      <Box sx={styles.dashboardContent}>
        <Box sx={styles.sidebar}>
          <DashboardSidebar />
        </Box>
        <Box sx={styles.outlet}>
          <Outlet />
        </Box>
      </Box>
    </Box>
  );
};
