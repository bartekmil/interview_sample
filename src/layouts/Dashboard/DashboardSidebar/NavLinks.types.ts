import { AppRoute } from 'components/ProtectedRoute/AppRoute';

export interface NavLinksTypes {
  title: string;
  path: AppRoute;
  icon: string;
}
