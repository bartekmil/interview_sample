import { AppRoute } from 'components/ProtectedRoute/AppRoute';

import { NavLinksTypes } from './NavLinks.types';

export const navLinks: NavLinksTypes[] = [
  {
    title: 'Dashboard',
    path: AppRoute.default,
    icon: 'bi:house-door',
  },
  {
    title: 'Jobs',
    path: AppRoute.jobs,
    icon: 'akar-icons:chat-bubble',
  },
  {
    title: 'Calendar',
    path: AppRoute.calendar,
    icon: 'akar-icons:calendar',
  },
  {
    title: 'Candidates',
    path: AppRoute.candidates,
    icon: 'akar-icons:person',
  },
];
