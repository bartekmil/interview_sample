import { Icon } from '@iconify/react';
import {
  Box,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import * as styles from 'layouts/Dashboard/Dashboard.styles';
import React from 'react';
import { matchPath, NavLink, useLocation } from 'react-router-dom';

import { navLinks } from './NavLinks';
import { NavLinksTypes } from './NavLinks.types';

export const DashboardSidebar = () => {
  const { pathname } = useLocation();
  const match = (path: string) =>
    path ? !!matchPath({ path, caseSensitive: true }, pathname) : false;

  return (
    <List component="div" disablePadding>
      {navLinks.map((link: NavLinksTypes): JSX.Element => {
        return (
          <ListItemButton
            selected={match(link.path)}
            component={NavLink}
            to={link.path}
            key={link.title}
          >
            <ListItemIcon>
              <Box component={Icon} icon={link.icon} sx={styles.navIcon} />
            </ListItemIcon>
            <ListItemText primary={link.title} />
          </ListItemButton>
        );
      })}
    </List>
  );
};
