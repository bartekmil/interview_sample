import { Person } from '@mui/icons-material';
import { Box, IconButton, Typography } from '@mui/material';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import * as styles from 'layouts/Dashboard/Dashboard.styles';
import { Link as RouterLink } from 'react-router-dom';

export const DashboardNavbar = () => {
  return (
    <Box sx={styles.dashboardNavbarContainer}>
      <Typography component="span" variant="h4">
        HR_BOARD
      </Typography>

      <IconButton size="large" component={RouterLink} to={AppRoute.profile}>
        <Person />
      </IconButton>
    </Box>
  );
};
