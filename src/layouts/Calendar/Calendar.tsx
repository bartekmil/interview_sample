import { Box, Paper } from '@mui/material';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';
import { CalendarHeader } from 'views/Calendar/CalendarHeader/CalendarHeader';
import { Day } from 'views/Calendar/Day/Day';
import { Month } from 'views/Calendar/Month/Month';
import { SidebarCalendar } from 'views/Calendar/SidebarCalendar/SidebarCalendar';

import * as styles from './Calendar.styles';

export const Calendar = () => {
  const { calendarViewType } = useCalendar();
  const isDayViewType = calendarViewType === 'Day';
  return (
    <Paper sx={styles.paperStyleCalendar}>
      <SidebarCalendar />
      <Box sx={styles.rightSideCalendar}>
        <CalendarHeader />
        {isDayViewType && <Day />}
        {!isDayViewType && <Month />}
      </Box>
    </Paper>
  );
};
