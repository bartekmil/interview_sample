import { Styles } from 'styles/theme';

export const paperStyleCalendar: Styles = {
  display: 'flex',
  flexDirection: 'row',
  height: '90vh',
  overflow: 'hidden',
};
export const rightSideCalendar: Styles = {
  display: 'flex',
  flexDirection: 'column',
  padding: 2,
  flexGrow: 1,
};
