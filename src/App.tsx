import './App.css';

import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { Calendar } from 'layouts/Calendar/Calendar';
import { Candidates } from 'layouts/Candidates/Candidates';
import { Dashboard } from 'layouts/Dashboard/Dashboard';
import { DashboardApp } from 'layouts/DashboardApp/DashboardApp';
// import { Dashboard } from 'layouts/Dashboard/Dashboard';
import { Jobs } from 'layouts/Jobs/Jobs';
import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { AddCandidateForm } from 'views/Candidates/AddCandidateForm/AddCandidateForm';
import { CandidateView } from 'views/Candidates/CandidateView/CandidateView';
import { EditCandidateForm } from 'views/Candidates/EditCandidateForm/EditCandidateForm';
import { AddJobForm } from 'views/Job/AddJobForm/AddJobForm';
import { JobOfferView } from 'views/Job/JobOfferView/JobOfferView';
import { NoMatchRoute } from 'views/NoMatchRoute/NoMatchRoute';
import { Profile } from 'views/Profile/Profile';

import { AppProviders } from './AppProviders/AppProviders';

function App() {
  return (
    <AppProviders>
      <Routes>
        <Route path={AppRoute.default} element={<Dashboard />}>
          <Route path={AppRoute.default} element={<DashboardApp />} />
          <Route path={AppRoute.profile} element={<Profile />} />
          <Route path={AppRoute.jobs} element={<Jobs />} />
          <Route path={`${AppRoute.jobs}/:id`} element={<JobOfferView />} />
          <Route path={AppRoute.addJobOffer} element={<AddJobForm />} />
          <Route path={AppRoute.candidates} element={<Candidates />} />
          <Route
            path={`${AppRoute.candidates}/:id`}
            element={<CandidateView />}
          />
          <Route
            path={`${AppRoute.candidates}/edit/:id`}
            element={<EditCandidateForm />}
          />
          <Route path={AppRoute.addCandidate} element={<AddCandidateForm />} />

          <Route path={AppRoute.calendar} element={<Calendar />} />
        </Route>
        <Route path="*" element={<NoMatchRoute />} />
      </Routes>
    </AppProviders>
  );
}

export default App;
