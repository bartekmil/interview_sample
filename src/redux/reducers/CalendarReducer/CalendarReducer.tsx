import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CalendarTypesViews } from 'views/Calendar/Calendar.types';
import { EventTypes } from 'views/Calendar/EventTypes.types';

import { InitialCalendarState } from './CalendarReducer.types';

const initialState: InitialCalendarState = {
  calendarViewType: CalendarTypesViews.Month,
  currentDate: new Date(),
  events: [],
};
export const calendarReducer = createSlice({
  name: 'calendar',
  initialState,
  reducers: {
    changeCalendarViewType: (
      state,
      action: PayloadAction<CalendarTypesViews>,
    ) => {
      state.calendarViewType = action.payload;
    },
    changeCurrentDate: (state, action: PayloadAction<Date>) => {
      state.currentDate = action.payload;
    },
    addEvent: (state, action: PayloadAction<EventTypes>) => {
      state.events = [...state.events, action.payload];
    },
  },
});
export const { changeCalendarViewType, changeCurrentDate, addEvent } =
  calendarReducer.actions;

export default calendarReducer.reducer;
