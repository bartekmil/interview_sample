import { CalendarTypesViews } from 'views/Calendar/Calendar.types';
import { EventTypes } from 'views/Calendar/EventTypes.types';

export interface InitialCalendarState {
  calendarViewType: CalendarTypesViews;
  currentDate: Date;
  events: EventTypes[];
}
