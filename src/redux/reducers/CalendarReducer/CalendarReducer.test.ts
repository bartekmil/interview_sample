import { CalendarTypesViews } from 'views/Calendar/Calendar.types';

import reducer, {
  addEvent,
  changeCalendarViewType,
  changeCurrentDate,
} from './CalendarReducer';

describe('check Calendar Reducer', () => {
  test('check state after change calendar view type', () => {
    const initialCurrentDate = new Date();
    const previousState = {
      calendarViewType: CalendarTypesViews.Month,
      currentDate: initialCurrentDate,
      events: [],
    };
    expect(
      reducer(previousState, changeCalendarViewType(CalendarTypesViews.Day)),
    ).toEqual({
      calendarViewType: CalendarTypesViews.Day,
      currentDate: initialCurrentDate,
      events: [],
    });
  });
  test('check state after change current date', () => {
    const initialCurrentDate = new Date();
    const previousState = {
      calendarViewType: CalendarTypesViews.Month,
      currentDate: initialCurrentDate,
      events: [],
    };
    const newChangedDate = new Date('2022-02-18');
    expect(reducer(previousState, changeCurrentDate(newChangedDate))).toEqual({
      calendarViewType: CalendarTypesViews.Month,
      currentDate: newChangedDate,
      events: [],
    });
  });
  test('check state after add Event', () => {
    const initialCurrentDate = new Date();
    const previousState = {
      calendarViewType: CalendarTypesViews.Month,
      currentDate: initialCurrentDate,
      events: [],
    };
    const newEvent = {
      title: 'test',
      startDate: new Date().toISOString().substring(0, 10),
      endDate: new Date().toISOString().substring(0, 10),
      allDay: true,
      description: 'test description',
      guests: {
        id: 1,
        name: 'Adam Falek',
        position: 'Senior software engineer',
        date: '20-01-2022',
        shortDescription: 'Lorem ipsum cost matm...',
        longDescription: 'Lorem ipsum cost matm...blblblbl b blb lbl',
        logo: 'https://e7.pngegg.com/pngimages/592/704/png-clipart-google-logo-google-search-google-cloud-platform-google-text-bing-thumbnail.png',
        companyName: 'Google',
      },
      jobs: {
        companyName: 'test',
        title: 'test',
        shortDescription: 'test',
        date: '2022-09-10',
        longDescription: 'test',
        logo: 'test',
        id: 1662824631648,
      },
      startTime: '12:00',
      endTime: '12:00',
      color: '29ec77',
    };
    expect(reducer(previousState, addEvent(newEvent))).toEqual({
      calendarViewType: CalendarTypesViews.Month,
      currentDate: initialCurrentDate,
      events: [newEvent],
    });
  });
});
