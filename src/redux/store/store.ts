import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { calendarReducer } from 'redux/reducers/CalendarReducer/CalendarReducer';

export const store = configureStore({
  reducer: {
    calendar: calendarReducer.reducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
