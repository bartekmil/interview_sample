import {
  useAppDispatch,
  useAppSelector,
} from 'redux/hooks/reduxHooks/reduxHooks';
import {
  addEvent,
  changeCalendarViewType,
  changeCurrentDate,
} from 'redux/reducers/CalendarReducer/CalendarReducer';
import { CalendarTypesViews } from 'views/Calendar/Calendar.types';
import { EventTypes } from 'views/Calendar/EventTypes.types';

export const useCalendar = () => {
  const { calendarViewType, currentDate, events } = useAppSelector(
    (state) => state.calendar,
  );
  const dispatch = useAppDispatch();
  const dispatchAddEvent = (dataEvent: EventTypes) => {
    dispatch(addEvent(dataEvent));
  };
  const dispatchChangeCurrentDate = (newDate: Date) => {
    dispatch(changeCurrentDate(newDate));
  };
  const dispatchChangeCalendarViewType = (type: CalendarTypesViews) => {
    dispatch(changeCalendarViewType(type));
  };
  return {
    calendarViewType,
    currentDate,
    events,
    dispatchAddEvent,
    dispatchChangeCurrentDate,
    dispatchChangeCalendarViewType,
  };
};
