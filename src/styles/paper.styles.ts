import { Styles } from 'styles/theme';

export const paperStyle: Styles = {
  maxWidth: '350px',
  display: 'flex',
  flexDirection: 'column',
  padding: 2.5,
};
export const centeringPaperContainer: Styles = {
  display: 'grid',
  placeItems: 'center',
  height: '100vh',
};
