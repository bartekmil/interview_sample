import { useContext } from 'react';

export const useSafeContextFactory = (
  name: string,
  context: React.Context<any>,
) => {
  const ctx = useContext(context);
  if (!ctx)
    throw new Error(`${name} can be used only inside ${name}ContextProvider`);
  return ctx;
};
