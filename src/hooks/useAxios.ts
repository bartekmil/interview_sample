import { useCallback, useEffect, useState } from 'react';

import { axiosInstance } from '../Axios/Axios';
import { axiosPayload } from './useAxios.types';

export const useAxios = <T, U = never>({
  url,
  method,
  initialFetch = true,
}: axiosPayload) => {
  const [data, setData] = useState<T>();
  const [error, setError] = useState('');
  const [isLoading, setIsLoading] = useState(true);
  const fetchData = useCallback(
    async (params?: U) => {
      setIsLoading(true);
      try {
        const response = await axiosInstance(url, {
          method: method,
          data: params,
        });
        const data = await response.data;
        setData(data);
        if (!initialFetch) return data;
      } catch (e) {
        setData(undefined);
        setError('Something went wrong');
      }
      setIsLoading(false);
    },
    [initialFetch, url, method],
  );
  const removeElement = async (newUrl: string) => {
    try {
      await axiosInstance(newUrl, { method: 'delete' });
      return;
    } catch (e) {
      setError('Something went wrong');
    }
  };

  useEffect(() => {
    initialFetch && fetchData();
  }, [initialFetch, fetchData]);
  return { data, error, isLoading, fetchData, removeElement };
};
