export interface axiosPayload {
  url: string;
  method: 'get' | 'post' | 'delete' | 'put';
  initialFetch?: boolean;
}
