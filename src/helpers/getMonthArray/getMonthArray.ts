import { range } from 'helpers/range/range';

export const getMonthArray = (current: Date) => {
  const year = current.getFullYear();
  const month = current.getMonth() + 1;
  const firstDayCurrentMonth = new Date(year, month, 1).getDay();
  let currentMonthCount = 0 - firstDayCurrentMonth;

  const daysMatrix = range(5, []).map(() => {
    return range(7).map(() => {
      currentMonthCount += 1;
      const actualDate = new Date(year, month, currentMonthCount);
      const objectDate = {
        day: actualDate.getDate(),
        month: actualDate.getMonth(),
        year: actualDate.getFullYear(),
      };
      return objectDate;
    });
  });
  return daysMatrix;
};
