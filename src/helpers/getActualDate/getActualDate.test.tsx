import { getActualDate } from './getActualDate';

describe('checkGetActualDate', () => {
  beforeAll(() => {
    jest.useFakeTimers();
    jest.setSystemTime(new Date('15 Jul 2022 02:12:00 GMT+0200').getTime());
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  test('Put another date to function', () => {
    expect(getActualDate()).toEqual('2022-07-15');
  });
});
