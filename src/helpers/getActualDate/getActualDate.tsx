export const getActualDate = () => {
  const date = new Date();
  const year = date.getFullYear();
  const monthWithOffset = date.getMonth() + 1;
  const month =
    monthWithOffset.toString().length < 2
      ? `0${monthWithOffset}`
      : monthWithOffset;
  const day =
    date.getDate().toString().length < 2
      ? `0${date.getDate()}`
      : date.getDate();

  const defaultDate = [year, month, day].join('-');
  return defaultDate;
};
