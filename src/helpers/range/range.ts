export const range = <T>(numberOfElements: number, content?: T) => {
  return new Array(numberOfElements)
    .fill(content)
    .map((value, i) => (value ? value : i));
};
