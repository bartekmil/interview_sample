import { range } from './range';

describe('range function', () => {
  it('check range function without second argument', () => {
    expect(range(3)).toEqual([0, 1, 2]);
  });
  it('check range function with second argument string ', () => {
    expect(range(3, 'test')).toEqual(['test', 'test', 'test']);
  });
  it('check range function with second argument array', () => {
    expect(range(3, [])).toEqual([[], [], []]);
  });
});
