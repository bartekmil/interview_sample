export interface ArithmeticOperations {
  addition: (x: number, y: number) => number;
  substraction: (x: number, y: number) => number;
}
