import { ArithmeticOperations } from './arithmeticOperations.types';

export const arithmeticOperations: ArithmeticOperations = {
  addition: function (x: number, y: number) {
    return x + y;
  },
  substraction: function (x: number, y: number) {
    return x - y;
  },
};
