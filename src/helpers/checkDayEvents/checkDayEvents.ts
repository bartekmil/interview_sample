import { EventTypes } from 'views/Calendar/EventTypes.types';

export const checkDayEvents = (events: EventTypes[], dateToCheck: Date) => {
  const eventsDay = events.filter((event) => {
    const startDayDate = new Date(event.startDate);
    startDayDate.setHours(0, 0, 0, 0);
    const endDayDate = new Date(event.endDate);
    endDayDate.setHours(24, 0, 0, 0);
    return dateToCheck >= startDayDate && dateToCheck <= endDayDate;
  });

  return eventsDay;
};
