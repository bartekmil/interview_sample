import { KeyTypesFunction } from './filterArrayByText.types';

export const filterArrayByText = <T extends KeyTypesFunction>(
  arrayData: T[],
  columnOnSearch: string,
  searchText: string,
): T[] => {
  return arrayData.filter((row) => row[columnOnSearch].includes(searchText));
};
