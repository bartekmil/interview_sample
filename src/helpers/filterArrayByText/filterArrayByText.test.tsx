import { filterArrayByText } from './filterArrayByText';

describe('checkFilterArrayBySearchText', () => {
  it('check function with array, columnOnSearch and Search Test', () => {
    const inputArray = [
      { id: 1, name: 'alex' },
      { id: 2, name: 'andrzej' },
      { id: 3, name: 'michal' },
    ];
    const columnOnSearch = 'name';
    const searchText = 'alex';
    expect(filterArrayByText(inputArray, columnOnSearch, searchText)).toEqual([
      { id: 1, name: 'alex' },
    ]);
  });
});
