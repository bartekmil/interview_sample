import { CircularProgress, Typography } from '@mui/material';
import { Card } from 'components/Card/Card';
import { useJob } from 'context/JobContext/useJob';
import { useEffect } from 'react';

export const OpenPositionsRaport = () => {
  const { data, isLoading, error } = useJob();

  if (isLoading) return <CircularProgress />;

  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return (
    <Card>
      <Card.Title>Open positions</Card.Title>
      <Card.Content>
        <Typography variant="h5" component="p">
          {Object.keys(data).length}
        </Typography>
      </Card.Content>
    </Card>
  );
};
