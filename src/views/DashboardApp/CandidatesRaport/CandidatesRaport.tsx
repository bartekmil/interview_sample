import { CircularProgress, Typography } from '@mui/material';
import { Card } from 'components/Card/Card';
import { useCandidate } from 'context/CandidatesContext/useCandidate';
import React from 'react';

export const CandidatesRaport = () => {
  const { data, isLoading, error } = useCandidate();
  if (isLoading) return <CircularProgress />;
  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return (
    <Card>
      <Card.Title>Candidates</Card.Title>
      <Card.Content>
        <Typography variant="h5" component="p">
          {data && data.length}
        </Typography>
      </Card.Content>
    </Card>
  );
};
