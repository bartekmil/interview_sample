import ComputerIcon from '@mui/icons-material/Computer';
import PersonIcon from '@mui/icons-material/Person';
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import { Box, CircularProgress, Typography } from '@mui/material';
import { Card } from 'components/Card/Card';
import MiniCard from 'components/MiniCard/MiniCard';
import { useJob } from 'context/JobContext/useJob';
import React from 'react';

import * as styles from './GeneralRaport.styles';

export const GeneralRaport = () => {
  const { data, isLoading, error } = useJob();
  if (isLoading && !data) return <CircularProgress />;

  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return (
    <Card>
      <Card.Title>Open positions</Card.Title>
      <Card.Content>
        <>
          <Typography display="inline" variant="h5" component="span">
            Total 48.5 % growth 😎 this month
          </Typography>
          <Box sx={styles.miniCardsContainer}>
            <MiniCard
              logo={<TrendingUpIcon />}
              colourLogo="blue"
              title={'Employees'}
              value={'245 k'}
            />
            <MiniCard
              logo={<PersonIcon />}
              colourLogo="green"
              title={'Jobs'}
              value={data.length}
            />
            <MiniCard
              logo={<ComputerIcon />}
              colourLogo="orange"
              title={'Interviews'}
              value={'1.54 k'}
            />
          </Box>
        </>
      </Card.Content>
    </Card>
  );
};
