import { Styles } from 'styles/theme';

export const miniCardsContainer: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
};
