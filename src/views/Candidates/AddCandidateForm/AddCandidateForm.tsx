import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { useAxios } from 'hooks/useAxios';
import { useState } from 'react';
import { SubmitHandler } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import { CandidateTypes } from '../Candidate.types';
import { CandidateForm } from '../CandidateForm/CandidateForm';

export const AddCandidateForm = () => {
  const [errorAdition, setErrorAdition] = useState('');

  const navigate = useNavigate();
  const { fetchData } = useAxios<unknown, CandidateTypes>({
    url: AppRoute.candidates,
    method: 'post',
    initialFetch: false,
  });

  const onSubmit: SubmitHandler<CandidateTypes> = async (data) => {
    try {
      await fetchData(data);
      navigate(AppRoute.candidates, { replace: true });
    } catch (_error) {
      setErrorAdition('Something wrong, please try again');
    }
  };
  return (
    <CandidateForm
      onSubmit={onSubmit}
      errorSubmit={errorAdition}
      titleForm={'Add candidate'}
    />
  );
};
