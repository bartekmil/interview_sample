import { CircularProgress, Typography } from '@mui/material';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { assertValueIsNonNullable } from 'helpers/assertValueIsNonNullable/assertValueIsNonNullable';
import { useAxios } from 'hooks/useAxios';
import { useParams } from 'react-router-dom';

import { CandidateTypes } from '../Candidate.types';
import { CandidateDetails } from '../CandidateDetails/CandidateDetails';

export const CandidateView = () => {
  const { id } = useParams();
  assertValueIsNonNullable(id, 'Unable to get id element');
  const { data, error, isLoading } = useAxios<CandidateTypes>({
    url: `${AppRoute.candidates}/${id}`,
    method: 'get',
  });
  if (isLoading) return <CircularProgress />;

  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return <CandidateDetails {...data} />;
};
