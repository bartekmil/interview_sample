import { Box, Button } from '@mui/material';
import { NavbarActionSelect } from 'components/NavbarActionSelect/NavbarActionSelect';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { SearchComponent } from 'components/SearchComponent/SearchComponent';
import { useCandidate } from 'context/CandidatesContext/useCandidate';
import { Link as RouterLink } from 'react-router-dom';

import * as styles from './Candidate.styles';

export const CandidateNavbar = () => {
  const { selectedRows, deleteCandidate, setSearchText, searchText } =
    useCandidate();

  return (
    <Box sx={styles.candidateNavbarContainer}>
      <NavbarActionSelect
        selectedRows={selectedRows}
        deleteAction={deleteCandidate}
      />
      <SearchComponent searchText={searchText} setSearchText={setSearchText} />

      <Button
        variant="contained"
        component={RouterLink}
        to={AppRoute.addCandidate}
        color="primary"
      >
        ADD CANDIDATE
      </Button>
    </Box>
  );
};
