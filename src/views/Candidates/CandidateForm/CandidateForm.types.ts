import { SubmitHandler } from 'react-hook-form';

import { CandidateTypes } from '../Candidate.types';

export interface CandidateFormTypes {
  onSubmit: SubmitHandler<CandidateTypes>;
  errorSubmit: string;
  initialData?: CandidateTypes;
  titleForm: string;
}
