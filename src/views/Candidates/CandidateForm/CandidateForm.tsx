import { Box, Button, Paper, TextField, Typography } from '@mui/material';
import { getActualDate } from 'helpers/getActualDate/getActualDate';
import { useForm } from 'react-hook-form';

import { CandidateTypes } from '../Candidate.types';
import * as styles from './CandidateForm.styles';
import { CandidateFormTypes } from './CandidateForm.types';

export const CandidateForm = ({
  onSubmit,
  errorSubmit,
  initialData,
  titleForm,
}: CandidateFormTypes) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<CandidateTypes>({
    defaultValues: initialData,
  });
  return (
    <Paper sx={styles.paperContainer}>
      <Typography variant="h5" component="span">
        {titleForm}
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box sx={styles.firstRowAdd}>
          <TextField
            {...register('name', {
              required: 'This field is required',
            })}
            label="Name"
            type="text"
            variant="outlined"
            error={Boolean(errors.name)}
            fullWidth
          />

          <TextField
            {...register('position', {
              required: 'This field is required',
            })}
            label="Position"
            type="text"
            variant="outlined"
            error={Boolean(errors.position)}
            fullWidth
          />

          <TextField
            {...register('shortDescription', {
              required: 'This field is required',
            })}
            label="Short description"
            type="text"
            variant="outlined"
            error={Boolean(errors.shortDescription)}
            fullWidth
          />
        </Box>
        <TextField
          {...register('date', {
            required: 'This field is required',
          })}
          id="date"
          type="date"
          defaultValue={getActualDate()}
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
        />

        <TextField
          {...register('longDescription', {
            required: 'This field is required',
          })}
          label="Long description"
          variant="outlined"
          margin="dense"
          minRows={4}
          error={Boolean(errors.longDescription)}
          fullWidth
          multiline
        />
        <TextField
          {...register('logo', {
            required: 'This field is required',
          })}
          label="Logo"
          type="text"
          variant="outlined"
          margin="dense"
          error={Boolean(errors.logo)}
          fullWidth
        />
        <TextField
          {...register('companyName', {
            required: 'This field is required',
          })}
          label="Company Name"
          type="text"
          variant="outlined"
          error={Boolean(errors.companyName)}
          fullWidth
        />

        <Box>
          <Button
            variant="contained"
            size="large"
            color="primary"
            type="submit"
          >
            Save
          </Button>
        </Box>
      </form>
      {errorSubmit && (
        <Typography color="error" variant="subtitle2">
          {errorSubmit}
        </Typography>
      )}
    </Paper>
  );
};
