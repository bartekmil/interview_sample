export interface CandidateTypes {
  name: string;
  position: string;
  date: string;
  shortDescription: string;
  longDescription: string;
  logo: string;
  companyName: string;
}
export interface CandidateTypesWithId extends CandidateTypes {
  id: number;
}
