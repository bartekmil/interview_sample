import { CircularProgress, Typography } from '@mui/material';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { assertValueIsNonNullable } from 'helpers/assertValueIsNonNullable/assertValueIsNonNullable';
import { useAxios } from 'hooks/useAxios';
import React, { useState } from 'react';
import { SubmitHandler } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';

import { CandidateTypes } from '../Candidate.types';
import { CandidateForm } from '../CandidateForm/CandidateForm';

export const EditCandidateForm = () => {
  const [errorEdition, setErrorEdition] = useState('');
  const { id } = useParams();
  assertValueIsNonNullable(id, 'Unable to get id element');

  const navigate = useNavigate();
  const { data, error, isLoading } = useAxios<CandidateTypes>({
    url: `${AppRoute.candidates}/${id}`,
    method: 'get',
  });
  const { fetchData } = useAxios<unknown, CandidateTypes>({
    url: AppRoute.candidates,
    method: 'put',
    initialFetch: false,
  });

  const onSubmit: SubmitHandler<CandidateTypes> = async (dataSubmit) => {
    try {
      await fetchData(dataSubmit);
      navigate(AppRoute.candidates, { replace: true });
    } catch (_error) {
      setErrorEdition('Something wrong, please try again');
    }
  };
  if (isLoading) return <CircularProgress />;

  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return (
    <CandidateForm
      onSubmit={onSubmit}
      errorSubmit={errorEdition}
      initialData={data}
      titleForm="Edit candidate"
    />
  );
};
