import { Avatar, Box, Paper, Typography } from '@mui/material';

import { CandidateTypes } from '../Candidate.types';
import * as styles from './CandidateDetails.styles';

export const CandidateDetails = ({
  companyName,
  date,
  logo,
  longDescription,
  shortDescription,
  name,
}: CandidateTypes) => {
  return (
    <Paper sx={styles.paperCandidateContainer}>
      <Box sx={styles.navbarCandidateDetails}>
        <Box sx={styles.navbarCandidateDetails}>
          <Avatar alt={name} src={logo} />
          <Typography variant="h3" component="span">
            {name}
          </Typography>
        </Box>
        <Typography variant="h2" component="span">
          {companyName}
        </Typography>

        <Typography variant="subtitle1" component="span">
          {date}
        </Typography>
      </Box>
      <Box>
        <Typography variant="body1" component="p">
          {longDescription}
        </Typography>
        <Typography variant="body1" component="p">
          {shortDescription}
        </Typography>
      </Box>
    </Paper>
  );
};
