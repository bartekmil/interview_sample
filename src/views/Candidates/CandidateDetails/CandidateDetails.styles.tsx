import { Styles } from 'styles/theme';

export const paperCandidateContainer: Styles = {
  width: '100%',
  padding: 2.5,
};
export const navbarCandidateDetails: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'row',
};
