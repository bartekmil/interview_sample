import { CircularProgress, Typography } from '@mui/material';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { TableTemplate } from 'components/TableTemplate/TableTemplate';
import { useCandidate } from 'context/CandidatesContext/useCandidate';
import React from 'react';

const headCellsCandidateMap = {
  User: 'name',
  Date: 'date',
  Position: 'position',
  Actions: 'actions',
};

export const CandidateTable = () => {
  const {
    data,
    selectedRows,
    setSelectedRows,
    deleteCandidate,
    searchText,
    isLoading,
    error,
  } = useCandidate();
  if (isLoading) return <CircularProgress />;
  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return (
    <TableTemplate
      functionOnIconBin={deleteCandidate}
      searchText={searchText}
      data={data}
      headCells={headCellsCandidateMap}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      columnOnSearch="name"
      routeName={AppRoute.candidates}
      buttonEditExists={true}
    />
  );
};
