import { Styles } from 'styles/theme';

export const dayBox: Styles = {
  border: '1px solid lightgrey',
  display: 'flex',
  flexDirection: 'column',
};
export const headerDay: Styles = {
  display: 'flex',
  justifyContent: 'flex-start',
  paddingLeft: 1,
};
export const contentDay: Styles = {
  flexShrink: 1,
  flexGrow: 1,
  flexBasis: 'auto',
};
export const actualDayBox: Styles = {
  backgroundColor: 'lightgrey',
  display: 'flex',
  flexDirection: 'column',
};

export const dayContentFirstTwo: Styles = {
  display: 'flex',
  flexDirection: 'column',
};
export const dayContentElement: Styles = {
  display: 'flex',
  justifyContent: 'center',
  margin: 0.5,
};
export const colorElement = (color: string, allDay: boolean) => {
  if (allDay) {
    return { backgroundColor: `#${color}` };
  }
  return { color: `#${color}` };
};
export const backgroundColorCurrentDay = (day: number, month: number) => {
  const actualDate = new Date();
  const actualDateMonth = actualDate.getMonth();
  const actualDateDay = actualDate.getDate();

  if (actualDateDay === day && actualDateMonth === month) {
    return actualDayBox;
  }
  return dayBox;
};
