export interface DateProps {
  dayElement: {
    day: number;
    month: number;
    year: number;
  };
}
