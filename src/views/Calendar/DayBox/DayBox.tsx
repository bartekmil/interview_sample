import { Box, Typography } from '@mui/material';
import { checkDayEvents } from 'helpers/checkDayEvents/checkDayEvents';
import React, { useMemo } from 'react';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';

import * as styles from './DayBox.styles';
import { DateProps } from './DayBox.types';

export const DayBox = ({ dayElement }: DateProps) => {
  const { day, month, year } = dayElement;
  const { events } = useCalendar();

  const dayEvents = useMemo(() => {
    const dateToCheck = new Date(`${year}-${month}-${day}`);
    const eventsDay = checkDayEvents(events, dateToCheck);
    return eventsDay;
  }, [day, events, month, year]);

  return (
    <Box sx={styles.backgroundColorCurrentDay(day, month)}>
      <Box sx={styles.headerDay}>
        <Typography component="span" variant="caption">
          {day}
        </Typography>
      </Box>
      <Box sx={styles.contentDay}>
        <Box sx={styles.dayContentFirstTwo}>
          {dayEvents.slice(0, 2).map((event) => (
            <Typography
              sx={styles.dayContentElement}
              component="span"
              variant="caption"
              key={event.color + event.title}
              style={styles.colorElement(event.color, event.allDay)}
            >
              {event.allDay && event.title}
              {!event.allDay && `${event.startTime}  ${event.title}`}
            </Typography>
          ))}
        </Box>
        <Box>
          {dayEvents.length > 2 && (
            <Typography
              sx={styles.dayContentElement}
              component="span"
              variant="caption"
            >
              Jeszcze {dayEvents.length - 2}
            </Typography>
          )}
        </Box>
      </Box>
    </Box>
  );
};
