import { Styles } from 'styles/theme';

export const monthGrid: Styles = {
  display: 'grid',
  gridTemplateRows: 'repeat(5,minmax(0,1fr))',
  gridTemplateColumns: 'repeat(7,minmax(0,1fr))',
  flex: '1 1 0%',
};
export const dayNamesGrid: Styles = {
  display: 'grid',
  gridTemplateRows: 'repeat(1,minmax(0,1fr))',
  gridTemplateColumns: 'repeat(7,minmax(0,1fr))',
  border: '1px solid lightGrey',
  borderBottom: '0px',
};
export const dayName: Styles = {
  display: 'flex',
  justifyContent: 'center',
  fontWeight: 'bold',
};
