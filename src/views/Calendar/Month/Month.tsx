import { Box } from '@mui/material';
import { getMonthArray } from 'helpers/getMonthArray/getMonthArray';
import React from 'react';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';
import { dayNames } from 'views/Calendar/dayNames';

import { DayBox } from '../DayBox/DayBox';
import * as styles from './Month.styles';

export const Month = () => {
  const { currentDate } = useCalendar();
  return (
    <>
      <Box sx={styles.dayNamesGrid}>
        {dayNames.map((day) => (
          <Box key={day} sx={styles.dayName}>
            {' '}
            {day}
          </Box>
        ))}
      </Box>
      <Box sx={styles.monthGrid}>
        {getMonthArray(currentDate).map((week, i) => (
          <React.Fragment key={i}>
            {week.map((dayElement) => (
              <React.Fragment key={dayElement.day + dayElement.month}>
                <DayBox dayElement={dayElement} />
              </React.Fragment>
            ))}
          </React.Fragment>
        ))}
      </Box>
    </>
  );
};
