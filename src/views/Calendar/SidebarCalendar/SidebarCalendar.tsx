import { Box, Button } from '@mui/material';
import React, { useCallback, useState } from 'react';

import { ModalAddEvent } from '../ModalAddEvent/ModalAddEvent';
import * as styles from './SidebarCalendar.styles';

export const SidebarCalendar = () => {
  const [openModalAddEvent, setOpenModalAddEvent] = useState(false);

  const handleCloseModal = useCallback(() => {
    setOpenModalAddEvent(false);
  }, []);
  return (
    <Box sx={styles.sidebarStyle}>
      <Button
        variant="contained"
        size="large"
        color="primary"
        type="submit"
        onClick={() => setOpenModalAddEvent(true)}
      >
        ADD EVENT
      </Button>
      {openModalAddEvent && (
        <ModalAddEvent
          openModalAddEvent={openModalAddEvent}
          handleCloseModal={handleCloseModal}
        />
      )}
    </Box>
  );
};
