import { SxProps, Theme } from '@mui/material';

export const sidebarStyle: SxProps<Theme> = {
  padding: 2.5,
  borderRight: '1px solid lightGrey',
};
