import { Styles } from 'styles/theme';

export const dayHoursGrid: Styles = {
  display: 'grid',
  gridTemplateColumns: '50px repeat(1,minmax(0,1fr))',
  overflowY: 'scroll',
};

export const hourBoxLegend: Styles = {
  minHeight: '48px',
  color: 'lightGrey',
  position: 'relative',
  paddingRight: '8px',
  display: 'flex',
  justifyContent: 'flex-end',
  top: '-6px',
};
export const dayContainer: Styles = {
  overflow: 'scroll',
};
export const circleAndAllDayEvents: Styles = {
  width: '100%',
};
export const headerTimeZone: Styles = {
  display: 'flex',
  color: 'lightGrey',
  paddingRight: '8px',
  alignItems: 'flex-end',
  width: '50px',
  borderBottom: '1px solid lightGrey',
};

export const circleWithDay: Styles = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  margin: 1,
};
export const dayCircle: Styles = {
  backgroundColor: '#1a73e8',
  color: 'white',
  borderRadius: '100%',
  fontSize: '25px',
  textAlign: 'center',
  width: '40px',
};
export const headerDay: Styles = {
  display: 'flex',
  flexDirection: 'row',
};
