import { Styles } from 'styles/theme';

export const circleWithDay: Styles = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  margin: 1,
};
export const dayCircle: Styles = {
  backgroundColor: '#1a73e8',
  color: 'white',
  borderRadius: '100%',
  fontSize: '25px',
  textAlign: 'center',
  width: '40px',
};
