import { Box, Typography } from '@mui/material';
import React from 'react';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';
import { dayNames } from 'views/Calendar/dayNames';

import * as styles from './ActualDayCircle.styles';

export const ActualDayCircle = () => {
  const { currentDate } = useCalendar();

  return (
    <Box sx={styles.circleWithDay}>
      <Typography variant="caption" component={'span'}>
        {dayNames[currentDate.getDay()]}.
      </Typography>
      <Box sx={styles.dayCircle}>{currentDate.getDate()}</Box>
    </Box>
  );
};
