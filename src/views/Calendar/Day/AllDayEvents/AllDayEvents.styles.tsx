import { Styles } from 'styles/theme';

export const allDayEvents: Styles = {
  borderLeft: '1px solid lightGrey',
  padding: 0.5,
  width: '50%',
};
