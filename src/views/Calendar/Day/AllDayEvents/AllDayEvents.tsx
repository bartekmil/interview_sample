import { Box, Typography } from '@mui/material';
import { checkDayEvents } from 'helpers/checkDayEvents/checkDayEvents';
import React from 'react';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';

import * as styles from './AllDayEvents.styles';

export const AllDayEvents = () => {
  const { currentDate, events } = useCalendar();

  return (
    <Box sx={styles.allDayEvents}>
      {checkDayEvents(events, currentDate)
        .filter((event) => event.allDay)
        .map((event) => (
          <Box
            key={event.color + event.title}
            style={{ backgroundColor: `#${event.color}` }}
          >
            <Typography variant="caption" component="span">
              {event.title}
            </Typography>
          </Box>
        ))}
    </Box>
  );
};
