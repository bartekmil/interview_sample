import { Box, Typography } from '@mui/material';

import { ActualDayCircle } from './ActualDayCircle/ActualDayCircle';
import { AllDayEvents } from './AllDayEvents/AllDayEvents';
import * as styles from './Day.styles';
import { HourEvents } from './HourEvents/HourEvents';
import { HourLegend } from './HourLegend/HourLegend';

const timeZone = new Date().getTimezoneOffset() / -60;

export const Day = () => {
  return (
    <Box sx={styles.dayContainer}>
      <Box sx={styles.headerDay}>
        <Box sx={styles.headerTimeZone}>
          <Typography variant="caption" component={'span'}>
            {`GMT+${timeZone}`}
          </Typography>
        </Box>
        <Box sx={styles.circleAndAllDayEvents}>
          <ActualDayCircle />
          <AllDayEvents />
        </Box>
      </Box>
      <Box sx={styles.dayHoursGrid}>
        <HourLegend />
        <HourEvents />
      </Box>
    </Box>
  );
};
