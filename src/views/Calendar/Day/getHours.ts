export const getHours = () => {
  const rows = [];
  for (let i = 0; i < 24; i++) {
    rows.push(`${i.toString().padStart(2, '0')}:00`);
  }
  return rows;
};
