import { Styles } from 'styles/theme';

export const hourBoxLegend: Styles = {
  minHeight: '48px',
  color: 'lightGrey',
  position: 'relative',
  paddingRight: '8px',
  display: 'flex',
  justifyContent: 'flex-end',
  top: '-6px',
};
