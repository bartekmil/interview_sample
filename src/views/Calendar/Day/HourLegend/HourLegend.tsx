import { Box, Typography } from '@mui/material';
import React from 'react';

import { getHours } from '../getHours';
import * as styles from './HourLegens.styles';

export const HourLegend = () => {
  return (
    <Box>
      {getHours().map((hour) => (
        <Box sx={styles.hourBoxLegend} key={hour}>
          <Typography variant="caption" component={'span'}>
            {' '}
            {hour}
          </Typography>
        </Box>
      ))}
    </Box>
  );
};
