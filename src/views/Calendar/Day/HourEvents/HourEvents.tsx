import { Box, Typography } from '@mui/material';
import { checkDayEvents } from 'helpers/checkDayEvents/checkDayEvents';
import React from 'react';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';

import { getHours } from '../getHours';
import * as styles from './HourEvents.styles';

export const HourEvents = () => {
  const { currentDate, events } = useCalendar();

  return (
    <Box>
      {getHours().map((hour) => (
        <Box key={hour} sx={styles.hourBox}>
          {checkDayEvents(events, currentDate)
            .filter(
              (event) =>
                event.allDay === false &&
                event.startTime.slice(0, 2) === hour.slice(0, 2),
            )
            .map((event) => {
              return (
                <Typography
                  sx={styles.eventHour}
                  variant="caption"
                  component="span"
                  style={{ backgroundColor: `#${event.color}` }}
                >
                  {`${event.startTime} - ${event.endTime} ${event.title}`}
                </Typography>
              );
            })}
        </Box>
      ))}
    </Box>
  );
};
