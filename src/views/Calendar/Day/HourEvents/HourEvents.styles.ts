import { Styles } from 'styles/theme';

export const hourBox: Styles = {
  border: '1px solid lightGrey',
  borderBottom: '0px',
  minHeight: '48px',
  display: 'flex',
  flexDirection: 'row',
};
export const eventHour: Styles = {
  padding: 0.1,
  width: '100%',
  display: 'flex',
  alignItems: 'center',
};
