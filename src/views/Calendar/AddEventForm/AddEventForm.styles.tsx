import { SxProps, Theme } from '@mui/material';

export const FormContainer: SxProps<Theme> = {
  width: '100%',
  padding: 2.5,
  display: 'flex',
  justifyContent: 'space-between',
};
export const formFields: SxProps<Theme> = {
  display: 'flex',
  flexDirection: 'column',
  gap: 1,
};
