import {
  Autocomplete,
  Box,
  Button,
  CircularProgress,
  FormControlLabel,
  Switch,
  TextField,
  Typography,
} from '@mui/material';
import { useCandidate } from 'context/CandidatesContext/useCandidate';
import { JobOfferResult } from 'context/JobContext/JobProvider.types';
import { useJob } from 'context/JobContext/useJob';
import { getActualDate } from 'helpers/getActualDate/getActualDate';
import { useState } from 'react';
import { Controller, SubmitHandler, useForm } from 'react-hook-form';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';
import { CandidateTypesWithId } from 'views/Candidates/Candidate.types';

import { EventTypes } from '../EventTypes.types';
import { AddEventTypes } from '../ModalAddEvent/ModalAddEvent.types';
import * as styles from './AddEventForm.styles';

const initialFormValues: EventTypes = {
  title: '',
  startDate: new Date().toISOString().substring(0, 10),
  endDate: new Date().toISOString().substring(0, 10),
  allDay: true,
  description: '',
  guests: '',
  jobs: '',
  startTime: '12:00',
  endTime: '12:00',
  color: '',
};

export const AddEventForm = ({
  handleCloseModal,
}: Pick<AddEventTypes, 'handleCloseModal'>) => {
  const [errorAddEvent, setErrorAddEvent] = useState('');
  const {
    handleSubmit,
    register,
    reset,
    control,
    watch,
    formState: { errors },
  } = useForm<EventTypes>({ defaultValues: initialFormValues });
  const watchAllDayField = watch('allDay');

  const {
    data: dataCandidate,
    isLoading: isLoadingCandidate,
    error: errorCandidate,
  } = useCandidate();
  const { data: dataJob, isLoading: isLoadingJob, error: errorJob } = useJob();
  const { dispatchAddEvent } = useCalendar();
  const onSubmit: SubmitHandler<EventTypes> = async (data) => {
    const randomColor = Math.floor(Math.random() * 16777215).toString(16);
    const dataWithRandomColor = { ...data, color: randomColor };
    dispatchAddEvent(dataWithRandomColor);
    handleCloseModal();
  };

  if (isLoadingCandidate || isLoadingJob) return <CircularProgress />;

  if (errorCandidate || !dataCandidate || errorJob || !dataJob)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  const candidatesOptions: Array<CandidateTypesWithId> =
    Object.values(dataCandidate);
  const jobsOptions: Array<JobOfferResult> = Object.values(dataJob);

  return (
    <Box sx={styles.FormContainer}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <TextField
          {...register('title', {
            required: 'This field is required',
          })}
          label="Title"
          type="text"
          variant="outlined"
          error={Boolean(errors.title)}
          fullWidth
        />

        <TextField
          {...register('startDate', {
            required: 'This field is required',
          })}
          type="date"
          defaultValue={getActualDate()}
          InputLabelProps={{
            shrink: true,
          }}
          label="Start Date"
          error={Boolean(errors.startDate)}
          fullWidth
        />
        <TextField
          {...register('endDate', {
            required: 'This field is required',
          })}
          type="date"
          label="End Date"
          defaultValue={getActualDate()}
          InputLabelProps={{
            shrink: true,
          }}
          error={Boolean(errors.endDate)}
          fullWidth
        />

        <FormControlLabel
          control={<Switch {...register('allDay')} defaultChecked />}
          label="All Day"
        />

        {watchAllDayField === false && (
          <Box>
            <TextField
              {...register('startTime')}
              type="time"
              label="Start time"
              variant="outlined"
              margin="dense"
              minRows={4}
              error={Boolean(errors.startTime)}
            />
            <TextField
              {...register('endTime')}
              type="time"
              label="End time"
              variant="outlined"
              margin="dense"
              minRows={4}
            />
          </Box>
        )}
        <TextField
          {...register('description', {
            required: 'This field is required',
          })}
          label="Description"
          variant="outlined"
          margin="dense"
          minRows={4}
          error={Boolean(errors.description)}
          fullWidth
          multiline
        />

        <Controller
          name="jobs"
          control={control}
          rules={{ required: true }}
          render={({ field: { onChange } }) => (
            <Autocomplete
              onChange={(_, data) => {
                data && onChange(data);
              }}
              options={jobsOptions}
              getOptionLabel={(option) => option.title}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Jobs"
                  error={Boolean(errors.jobs)}
                />
              )}
            ></Autocomplete>
          )}
        />
        <Controller
          name="guests"
          control={control}
          rules={{ required: true }}
          render={({ field: { onChange } }) => (
            <Autocomplete
              onChange={(_, data) => {
                data && onChange(data);
              }}
              options={candidatesOptions}
              getOptionLabel={(option) => option.name}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Guests"
                  error={Boolean(errors.guests)}
                />
              )}
            ></Autocomplete>
          )}
        />
        <Box sx={styles.FormContainer}>
          <Button
            variant="contained"
            size="large"
            color="inherit"
            type="submit"
          >
            Save
          </Button>
          <Button
            variant="outlined"
            size="large"
            color="primary"
            onClick={() => reset(initialFormValues)}
          >
            Reset
          </Button>
        </Box>
      </form>
      {errorAddEvent && (
        <Typography color="error" variant="subtitle2">
          {errorAddEvent}
        </Typography>
      )}
    </Box>
  );
};
