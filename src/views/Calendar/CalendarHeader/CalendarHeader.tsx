import { Icon } from '@iconify/react';
import { Box, IconButton } from '@mui/material';
import { arithmeticOperations } from 'helpers/arithmeticOperations/arithmeticOperations';
import { ArithmeticOperations } from 'helpers/arithmeticOperations/arithmeticOperations.types';
import React, { useCallback } from 'react';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';

import { CalendarTypesViews } from '../Calendar.types';
import {
  buttonChangeNextPrevious,
  headerCalendarContainer,
} from './CalendarHeader.styles';
import { CalendarTypeButtons } from './CalendarTypeButtons/CalendarTypeButtons';
import { FullDate } from './FullDate/FullDate';

export const CalendarHeader = () => {
  const { calendarViewType, currentDate, dispatchChangeCurrentDate } =
    useCalendar();

  const changeDateFunc = useCallback(
    (operatorAction: keyof ArithmeticOperations) => () => {
      let newDate: Date = new Date(currentDate);
      if (calendarViewType === CalendarTypesViews.Day) {
        newDate = new Date(
          newDate.setDate(
            arithmeticOperations[operatorAction](newDate.getDate(), 1),
          ),
        );
      }
      if (calendarViewType === CalendarTypesViews.Month) {
        newDate = new Date(
          newDate.setMonth(
            arithmeticOperations[operatorAction](newDate.getMonth(), 1),
          ),
        );
      }
      dispatchChangeCurrentDate(newDate);
    },
    [currentDate, calendarViewType, dispatchChangeCurrentDate],
  );
  return (
    <Box sx={headerCalendarContainer}>
      <Box sx={buttonChangeNextPrevious}>
        <IconButton>
          <Box
            onClick={changeDateFunc('substraction')}
            component={Icon}
            icon={'dashicons:arrow-left-alt2'}
          />
        </IconButton>
        <IconButton onClick={changeDateFunc('addition')}>
          <Box component={Icon} icon={'dashicons:arrow-right-alt2'} />
        </IconButton>

        <FullDate
          calendarViewType={calendarViewType}
          currentDate={currentDate}
        />
      </Box>
      <Box>
        <CalendarTypeButtons />
      </Box>
    </Box>
  );
};
