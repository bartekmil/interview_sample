import { Typography } from '@mui/material';
import React from 'react';
import { CalendarTypesViews } from 'views/Calendar/Calendar.types';

const nameOfMonths = [
  'Styczeń',
  'Luty',
  'Marzec',
  'Kwiecień',
  'Maj',
  'Czerwiec',
  'Lipiec',
  'Sierpień',
  'Wrzesień',
  'Październik',
  'Listopad',
  'Grudzień',
];
export const FullDate = ({
  calendarViewType,
  currentDate,
}: {
  calendarViewType: CalendarTypesViews;
  currentDate: Date;
}) => {
  return (
    <Typography variant="h5" component="span">
      {calendarViewType === CalendarTypesViews.Day && currentDate.getDate()}{' '}
      {''}
      {nameOfMonths[currentDate.getMonth()]} {''}
      {currentDate.getFullYear()}
    </Typography>
  );
};
