import { Button } from '@mui/material';
import React, { useCallback } from 'react';
import { useCalendar } from 'redux/hooks/useCalendar/useCalendar';
import { CalendarTypesViews } from 'views/Calendar/Calendar.types';

import { checkActiveButton } from './CalendarTypeBurrons.styles';

function assertIsSelectValue(value: any): asserts value is CalendarTypesViews {
  const isCalendarType = Object.keys(CalendarTypesViews).includes(value);
  if (!isCalendarType) {
    throw new Error('Provided value is not CalendarTypeViews');
  }
}

export const CalendarTypeButtons = () => {
  const { calendarViewType, dispatchChangeCalendarViewType } = useCalendar();
  const handleChangeCalendarTypeView = useCallback(
    (type: CalendarTypesViews) => {
      dispatchChangeCalendarViewType(type);
    },
    [dispatchChangeCalendarViewType],
  );

  return (
    <>
      {Object.keys(CalendarTypesViews).map((type) => (
        <Button
          key={type}
          sx={checkActiveButton(calendarViewType === type)}
          onClick={() => {
            assertIsSelectValue(type);
            handleChangeCalendarTypeView(type);
          }}
          color="inherit"
          variant="outlined"
        >
          {type}
        </Button>
      ))}
    </>
  );
};
