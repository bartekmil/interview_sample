import { SxProps, Theme } from '@mui/material';

const activeButton: SxProps<Theme> = {
  backgroundColor: 'purple',
};

export const checkActiveButton = (active: boolean) => {
  if (active) {
    return activeButton;
  }
};
