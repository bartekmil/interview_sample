import { SxProps, Theme } from '@mui/material';

export const headerCalendarContainer: SxProps<Theme> = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '100%',
};
export const buttonChangeNextPrevious: SxProps<Theme> = {
  display: 'flex',
  alignItems: 'center',
};
