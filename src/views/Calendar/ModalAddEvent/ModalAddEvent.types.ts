export interface AddEventTypes {
  openModalAddEvent: boolean;
  handleCloseModal(): void;
}
