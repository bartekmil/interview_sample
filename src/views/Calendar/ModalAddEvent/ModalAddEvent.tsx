import { Box, IconButton, Modal, Typography } from '@mui/material';
import { CandidateProvider } from 'context/CandidatesContext/CandidateProvider';
import { JobProvider } from 'context/JobContext/JobProvider';
import React from 'react';

import { AddEventForm } from '../AddEventForm/AddEventForm';
import * as styles from './ModalAddEvent.styles';
import { AddEventTypes } from './ModalAddEvent.types';

export const ModalAddEvent = ({
  openModalAddEvent,
  handleCloseModal,
}: AddEventTypes) => {
  return (
    <Modal
      open={openModalAddEvent}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={styles.modal}>
        <Box sx={styles.modalHeader}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add Event
          </Typography>
          <IconButton onClick={handleCloseModal}>x</IconButton>
        </Box>
        <CandidateProvider>
          <JobProvider>
            <AddEventForm handleCloseModal={handleCloseModal} />
          </JobProvider>
        </CandidateProvider>
      </Box>
    </Modal>
  );
};
