import { JobOfferResult } from 'context/JobContext/JobProvider.types';
import { CandidateTypesWithId } from 'views/Candidates/Candidate.types';

export interface EventTypes {
  title: string;
  startDate: string;
  endDate: string;
  allDay: boolean;
  description: string;
  guests: string | CandidateTypesWithId;
  jobs: string | JobOfferResult;
  startTime: string;
  endTime: string;
  color: string;
}
