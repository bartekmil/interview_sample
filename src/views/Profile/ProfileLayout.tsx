import { Typography } from '@mui/material';

import { ProfileData } from './Profile.types';

export const ProfileLayout = ({
  firstname,
  lastname,
  username,
}: ProfileData) => {
  return (
    <>
      <Typography variant="h2" component="span">
        User Sign In
      </Typography>
      <Typography variant="body1" component="p">
        Your name:{firstname}
      </Typography>
      <Typography variant="body1" component="p">
        Your last name:{lastname}
      </Typography>
      <Typography variant="body1" component="p">
        Your email:{username}
      </Typography>
    </>
  );
};
