export interface ProfileData {
  firstname: string;
  lastname: string;
  username: string;
}
