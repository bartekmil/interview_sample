import { CircularProgress, Typography } from '@mui/material';
import { useAxios } from 'hooks/useAxios';

import { ProfileData } from './Profile.types';
import { ProfileLayout } from './ProfileLayout';

export const Profile = () => {
  const { data, error, isLoading } = useAxios<ProfileData>({
    url: '/profile',
    method: 'get',
  });
  if (isLoading) return <CircularProgress />;

  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return <ProfileLayout {...data} />;
};
