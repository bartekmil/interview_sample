export interface JobTypes {
  title: string;
  date: string;
  shortDescription: string;
  longDescription: string;
  logo: string;
  companyName: string;
}
