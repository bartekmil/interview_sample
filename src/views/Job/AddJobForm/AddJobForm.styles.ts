import { Styles } from 'styles/theme';

export const paperContainer: Styles = {
  width: '100%',
  padding: 2.5,
};
export const firstRowAddJob: Styles = {
  marginBottom: 1.25,
  flexDirection: 'row',
};
