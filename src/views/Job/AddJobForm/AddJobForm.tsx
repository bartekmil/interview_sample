import { Box, Button, Paper, TextField, Typography } from '@mui/material';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { getActualDate } from 'helpers/getActualDate/getActualDate';
import { useAxios } from 'hooks/useAxios';
import { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { JobTypes } from 'views/Job/Job.types';

import * as styles from './AddJobForm.styles';

export const AddJobForm = () => {
  const [errorRegister, setErrorRegister] = useState('');
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<JobTypes>();
  const navigate = useNavigate();
  const { fetchData } = useAxios<unknown, JobTypes>({
    url: AppRoute.jobs,
    method: 'post',
    initialFetch: false,
  });

  const onSubmit: SubmitHandler<JobTypes> = async (data) => {
    try {
      await fetchData(data);
      navigate(AppRoute.jobs, { replace: true });
    } catch (_error) {
      setErrorRegister('Something wrong, please try again');
    }
  };
  return (
    <Paper sx={styles.paperContainer}>
      <Typography variant="h5" component="span">
        Add new job
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box sx={styles.firstRowAddJob}>
          <TextField
            {...register('companyName', {
              required: 'This field is required',
            })}
            label="Company Name"
            type="text"
            variant="outlined"
            error={Boolean(errors.companyName)}
            fullWidth
          />

          <TextField
            {...register('title', {
              required: 'This field is required',
            })}
            label="Title"
            type="text"
            variant="outlined"
            error={Boolean(errors.title)}
            fullWidth
          />

          <TextField
            {...register('shortDescription', {
              required: 'This field is required',
            })}
            label="Short description"
            type="text"
            variant="outlined"
            error={Boolean(errors.shortDescription)}
            fullWidth
          />
        </Box>
        <TextField
          {...register('date', {
            required: 'This field is required',
          })}
          id="date"
          type="date"
          defaultValue={getActualDate()}
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
        />

        <TextField
          {...register('longDescription', {
            required: 'This field is required',
          })}
          label="Long description"
          variant="outlined"
          margin="dense"
          minRows={4}
          error={Boolean(errors.longDescription)}
          fullWidth
          multiline
        />
        <TextField
          {...register('logo', {
            required: 'This field is required',
          })}
          label="Logo"
          type="text"
          variant="outlined"
          margin="dense"
          error={Boolean(errors.logo)}
          fullWidth
        />

        <Box>
          <Button
            variant="contained"
            size="large"
            color="primary"
            type="submit"
          >
            Save
          </Button>
        </Box>
      </form>
      {errorRegister && (
        <Typography color="error" variant="subtitle2">
          {errorRegister}
        </Typography>
      )}
    </Paper>
  );
};
