import { CircularProgress, Typography } from '@mui/material';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { assertValueIsNonNullable } from 'helpers/assertValueIsNonNullable/assertValueIsNonNullable';
import { useAxios } from 'hooks/useAxios';
import { useParams } from 'react-router-dom';
import { JobTypes } from 'views/Job/Job.types';
import { JobOfferDetails } from 'views/Job/JobOfferDetails/JobOfferDetails';

export const JobOfferView = () => {
  const { id } = useParams();
  assertValueIsNonNullable(id, 'Unable to get id element');
  const { data, error, isLoading } = useAxios<JobTypes>({
    url: `${AppRoute.jobs}/${id}`,
    method: 'get',
  });
  if (isLoading) return <CircularProgress />;

  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return <JobOfferDetails {...data} />;
};
