import { Avatar, Box, Paper, Typography } from '@mui/material';
import { JobTypes } from 'views/Job/Job.types';

import * as styles from './JobOfferDetails.styles';

export const JobOfferDetails = ({
  companyName,
  date,
  logo,
  longDescription,
  shortDescription,
  title,
}: JobTypes) => {
  return (
    <Paper sx={styles.paperJobContainer}>
      <Box sx={styles.navbarJobDetails}>
        <Box sx={styles.navbarJobDetails}>
          <Avatar alt={companyName} src={logo} />
          <Typography variant="h3" component="span">
            {companyName}
          </Typography>
        </Box>
        <Typography variant="h2" component="span">
          {title}
        </Typography>

        <Typography variant="subtitle1" component="span">
          {date}
        </Typography>
      </Box>
      <Box>
        <Typography variant="body1" component="p">
          {longDescription}
        </Typography>
        <Typography variant="body1" component="p">
          {shortDescription}
        </Typography>
      </Box>
    </Paper>
  );
};
