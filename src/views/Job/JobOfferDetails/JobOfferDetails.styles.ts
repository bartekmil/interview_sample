import { Styles } from 'styles/theme';

export const paperJobContainer: Styles = {
  width: '100%',
  padding: 2.5,
};
export const navbarJobDetails: Styles = {
  justifyContent: 'space-between',
  alignItems: 'center',
  flexDirection: 'row',
};
