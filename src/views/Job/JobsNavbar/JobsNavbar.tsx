import { Box, Button, CircularProgress } from '@mui/material';
import { NavbarActionSelect } from 'components/NavbarActionSelect/NavbarActionSelect';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { SearchComponent } from 'components/SearchComponent/SearchComponent';
import { useJob } from 'context/JobContext/useJob';
import { Link as RouterLink } from 'react-router-dom';

import * as styles from './JobsNavbar.styles';

export const JobsNavbar = () => {
  const { selectedRows, deleteJobOffer, searchText, setSearchText, isLoading } =
    useJob();
  if (isLoading) return <CircularProgress />;

  return (
    <Box sx={styles.jobsNavbarContainer}>
      <NavbarActionSelect
        selectedRows={selectedRows}
        deleteAction={deleteJobOffer}
      />
      <SearchComponent searchText={searchText} setSearchText={setSearchText} />
      <Button
        variant="contained"
        component={RouterLink}
        to={AppRoute.addJobOffer}
        color="primary"
      >
        ADD JOB OFFER
      </Button>
    </Box>
  );
};
