import { Styles } from 'styles/theme';

export const jobsNavbarContainer: Styles = {
  padding: 2.5,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
};
