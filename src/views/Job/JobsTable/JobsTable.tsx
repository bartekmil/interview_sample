import { CircularProgress, Typography } from '@mui/material';
import { AppRoute } from 'components/ProtectedRoute/AppRoute';
import { TableTemplate } from 'components/TableTemplate/TableTemplate';
import { useJob } from 'context/JobContext/useJob';
import React from 'react';

const headCellsJobMap = {
  Position: 'title',
  Date: 'date',
  Actions: 'actions',
};

export const JobsTable = () => {
  const {
    data,
    selectedRows,
    setSelectedRows,
    deleteJobOffer,
    searchText,
    isLoading,
    error,
  } = useJob();
  if (isLoading) return <CircularProgress />;
  if (error || !data)
    return (
      <Typography component="span" color="error" role="alert">
        Something wrong
      </Typography>
    );
  return (
    <TableTemplate
      functionOnIconBin={deleteJobOffer}
      searchText={searchText}
      data={data}
      headCells={headCellsJobMap}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      columnOnSearch={'title'}
      routeName={AppRoute.jobs}
      buttonEditExists={false}
    />
  );
};
