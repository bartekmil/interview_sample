import { AppRoute } from 'components/ProtectedRoute/AppRoute';

export interface TableTemplateTypes<T> {
  routeName: AppRoute;
  data: T[];
  selectedRows: number[];
  headCells: { [key: string]: string };
  setSelectedRows: React.Dispatch<React.SetStateAction<number[]>>;
  searchText: string;
  functionOnIconBin: (ids: [number]) => void;
  columnOnSearch: string;
  buttonEditExists: boolean;
}

export interface KeyTypes {
  [key: string]: any;
  id: number;
}
