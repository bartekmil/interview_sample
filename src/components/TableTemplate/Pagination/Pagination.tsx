import { Icon } from '@iconify/react';
import {
  Box,
  IconButton,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
} from '@mui/material';
import { useCallback, useMemo } from 'react';

import * as styles from './Pagination.styles';
import { PaginationProps } from './Pagination.types';

const rowsPerPageOptions = [5, 10, 15, 20];

export const Pagination = ({
  dataLength,
  setCurrentPage,
  setRowsPerPage,
  rowsPerPage,
  currentPage,
}: PaginationProps) => {
  const handleChangeRowsPerPage = (event: SelectChangeEvent) => {
    setRowsPerPage(+event.target.value);
    setCurrentPage(1);
  };
  const handleClickLeftArrow = () => {
    setCurrentPage(currentPage - 1);
  };
  const handleClickRightArrow = () => {
    setCurrentPage(currentPage + 1);
  };
  const isLastPage = useMemo(
    () => currentPage === Math.floor(dataLength / rowsPerPage + 1),

    [dataLength, rowsPerPage, currentPage],
  );
  const getInformationRows = useCallback(() => {
    const startPage = (currentPage - 1) * rowsPerPage + 1;
    const endPage = Math.min(currentPage * rowsPerPage, dataLength);

    return `${startPage} -${endPage} of ${dataLength}`;
  }, [currentPage, rowsPerPage, dataLength]);

  return (
    <Box sx={styles.paginationContainer}>
      <Box sx={styles.boxRowsPerPage}>
        <Typography variant="subtitle1" component="span">
          Rows per page:
        </Typography>
        <Select
          value={String(rowsPerPage)}
          id="grouped-select"
          label="Grouping"
          onChange={handleChangeRowsPerPage}
        >
          {rowsPerPageOptions.map((option) => {
            return (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            );
          })}
        </Select>
      </Box>
      <Box>
        <Typography variant="subtitle1" component="span">
          {getInformationRows()}
        </Typography>
        <IconButton disabled={currentPage === 1} onClick={handleClickLeftArrow}>
          <Box component={Icon} icon={'dashicons:arrow-left-alt2'} />
        </IconButton>
        <IconButton disabled={isLastPage} onClick={handleClickRightArrow}>
          <Box component={Icon} icon={'dashicons:arrow-right-alt2'} />
        </IconButton>
      </Box>
    </Box>
  );
};
