export interface PaginationProps {
  rowsPerPage: number;
  currentPage: number;
  setRowsPerPage: (rowsPerPage: number) => void;
  setCurrentPage: (currentPage: number) => void;
  dataLength: number;
}
