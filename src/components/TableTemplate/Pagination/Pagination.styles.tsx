import { Styles } from 'styles/theme';

export const paginationContainer: Styles = {
  display: 'flex',
  justifyContent: 'flex-end',
  alignItems: 'center',
};
export const boxRowsPerPage: Styles = {
  display: 'flex',
  alignItems: 'center',
  marginRight: 2.5,
};
