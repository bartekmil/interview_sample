import { Icon } from '@iconify/react';
import {
  Box,
  Checkbox,
  IconButton,
  Link,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
} from '@mui/material';
import { filterArrayByText } from 'helpers/filterArrayByText/filterArrayByText';
import React, { useCallback, useMemo, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';

import { Pagination } from './Pagination/Pagination';
import * as styles from './TableTemplate.styles';
import { KeyTypes, TableTemplateTypes } from './TableTemplate.types';
import { TableTemplateHead } from './TableTemplateHead/TableTemplateHead';

export const TableTemplate: <T extends KeyTypes>(
  props: TableTemplateTypes<T>,
) => React.ReactElement<TableTemplateTypes<T>> = ({
  data,
  selectedRows,
  headCells,
  searchText,
  functionOnIconBin,
  columnOnSearch,
  routeName,
  buttonEditExists,
  setSelectedRows,
}) => {
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const sliceOffset = useMemo(
    () => (currentPage - 1) * rowsPerPage,
    [currentPage, rowsPerPage],
  );
  const sliceFinish = useMemo(
    () => rowsPerPage * currentPage,
    [rowsPerPage, currentPage],
  );

  const isSelected = useCallback(
    (id: number) => selectedRows.indexOf(id) !== -1,
    [selectedRows],
  );
  const onResetSelectedRows = useCallback(
    () => setSelectedRows([]),
    [setSelectedRows],
  );
  const onSelectRow = useCallback(
    (rowId: number) => (_e: React.MouseEvent) => {
      setSelectedRows((prevState) => {
        return prevState.includes(rowId)
          ? prevState.filter((r) => r !== rowId)
          : [...prevState, rowId];
      });
    },
    [setSelectedRows],
  );

  const onSelectAllRows = useCallback(
    (allRowsIds: number[]) => setSelectedRows(allRowsIds),
    [setSelectedRows],
  );
  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!data) return;
    if (event.target.checked) {
      const newSelecteds = Object.values(data).map((n) => n.id);
      onSelectAllRows(newSelecteds);
      return;
    }
    onResetSelectedRows();
  };
  const getDataToDisplay = useCallback(
    <T extends KeyTypes>(data: T[]): T[] => {
      return filterArrayByText(
        Object.values(data),
        columnOnSearch,
        searchText,
      ).slice(sliceOffset, sliceFinish);
    },
    [columnOnSearch, searchText, sliceFinish, sliceOffset],
  );
  return (
    <>
      <TableContainer sx={styles.tableTemplateSize}>
        <Table aria-labelledby="jobsTable" size={'medium'}>
          <TableTemplateHead
            headCells={headCells}
            onSelectAllClick={handleSelectAllClick}
          />
          <TableBody>
            {getDataToDisplay(data).map((row) => {
              const isItemSelected = isSelected(row.id);
              return (
                <TableRow
                  hover
                  role="checkbox"
                  tabIndex={-1}
                  key={row.id}
                  selected={isItemSelected}
                >
                  <TableCell padding="checkbox">
                    <Checkbox
                      color="primary"
                      onClick={onSelectRow(row.id)}
                      checked={isItemSelected}
                    />
                  </TableCell>

                  {Object.values(headCells).map((cell: string, index) => {
                    if (index === 0) {
                      return (
                        <TableCell scope="row" padding="none">
                          <Link
                            component={RouterLink}
                            to={`${routeName}/${row.id}`}
                          >
                            {row[Object.values(headCells)[0]]}
                          </Link>
                        </TableCell>
                      );
                    }
                    if (cell === 'actions') {
                      return (
                        <TableCell align="left">
                          <IconButton
                            onClick={() => functionOnIconBin([row.id])}
                          >
                            <Box component={Icon} icon={'icomoon-free:bin'} />
                          </IconButton>
                          <IconButton
                            component={RouterLink}
                            to={`${routeName}/${row.id}`}
                          >
                            <Box component={Icon} icon={'icomoon-free:eye'} />
                          </IconButton>
                          {buttonEditExists && (
                            <IconButton
                              component={RouterLink}
                              to={`${routeName}/edit/${row.id}`}
                            >
                              <Box component={Icon} icon={'eva:edit-outline'} />
                            </IconButton>
                          )}
                        </TableCell>
                      );
                    }
                    return <TableCell align="left">{row[cell]}</TableCell>;
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Pagination
        dataLength={Object.values(data).length}
        rowsPerPage={rowsPerPage}
        setRowsPerPage={setRowsPerPage}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
      />
    </>
  );
};
