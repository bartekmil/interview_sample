import { Checkbox, TableCell, TableHead, TableRow } from '@mui/material';

import { TableHeadProperties } from './TableTemplateHead.types';

export const TableTemplateHead = ({
  onSelectAllClick,
  headCells,
}: TableHeadProperties) => {
  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts',
            }}
          />
        </TableCell>
        {Object.keys(headCells).map((headCell) => (
          <TableCell key={headCell}>{headCell}</TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};
