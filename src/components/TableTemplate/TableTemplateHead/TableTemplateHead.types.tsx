export interface TableHeadProperties {
  headCells: { [key: string]: string };
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
}
export interface HeadCell {
  id: number;
}
