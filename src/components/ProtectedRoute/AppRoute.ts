export enum AppRoute {
  signIn = '/signin',
  signUp = '/signUp',
  default = '/',
  profile = '/profile',
  jobs = '/jobs',
  candidates = '/candidates',
  calendar = '/calendar',
  addJobOffer = '/jobs/add',
  addCandidate = '/candidates/add',
}
