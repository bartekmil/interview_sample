import { TextField } from '@mui/material';
import React from 'react';

import { SearchComponentTypes } from './SearchComponent.types';

export const SearchComponent = ({
  searchText,
  setSearchText,
}: SearchComponentTypes) => {
  const handleChangeSearchText = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setSearchText(event.target.value);
  };
  return (
    <TextField
      type="search"
      value={searchText}
      id="outlined-basic"
      onChange={handleChangeSearchText}
      label="Search"
      variant="outlined"
    />
  );
};
