export interface SearchComponentTypes {
  searchText: string;
  setSearchText: (text: string) => void;
}
