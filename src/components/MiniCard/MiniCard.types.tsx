import { ReactNode } from 'react';

export interface MiniCardProps {
  logo: ReactNode;
  colourLogo: string;
  title: string;
  value: string;
}
