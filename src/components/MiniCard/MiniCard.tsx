import { Box, Typography } from '@mui/material';
import React, { useMemo } from 'react';

import * as styles from './MiniCard.styles';
import { MiniCardProps } from './MiniCard.types';

const MiniCard = ({ logo, colourLogo, title, value }: MiniCardProps) => {
  const logoStyle = useMemo(() => {
    return {
      ...styles.logoStyle,
      backgroundColor: colourLogo,
    };
  }, [colourLogo]);
  return (
    <Box sx={styles.miniCardContainer}>
      <Box sx={logoStyle}>{logo}</Box>

      <Box>
        <Typography variant="body1" component="p">
          {title}
        </Typography>
        <Typography variant="h5" component="p">
          {value}
        </Typography>
      </Box>
    </Box>
  );
};

export default MiniCard;
