import { Styles } from 'styles/theme';

export const logoStyle: Styles = {
  width: '50px',
  height: '50px',
  marginRight: 1,
  '& > svg': {
    height: '100%',
    width: '100%',
  },
};
export const miniCardContainer: Styles = {
  display: 'flex',
  justifyContent: 'space-between',
  marginRight: 2.5,
};
