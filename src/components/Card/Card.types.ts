import { ReactNode } from 'react';

export type CardChildren = {
  children: ReactNode;
};
export type CardTitles = {
  children: ReactNode;
};
