import { Styles } from 'styles/theme';

export const CardContainerStyle: Styles = {
  minWidth: '350px',
  display: 'flex',
  flexDirection: 'column',
  padding: 2.5,
};
