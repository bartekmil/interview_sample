import React from 'react';

import { CardChildren } from '../Card.types';

export const CardContent = ({ children }: CardChildren) => {
  return <>{children}</>;
};
