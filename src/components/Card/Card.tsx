import { Paper } from '@mui/material';
import React from 'react';

import * as styles from './Card.styles';
import { CardChildren } from './Card.types';
import { CardContent } from './CardContent/CardContent';
import { CardTitle } from './CardTitle/CardTitle';

export const Card = ({ children }: CardChildren) => {
  return <Paper sx={styles.CardContainerStyle}>{children}</Paper>;
};

Card.Title = CardTitle;
Card.Content = CardContent;
