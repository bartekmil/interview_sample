import { Typography } from '@mui/material';
import React from 'react';

import { CardTitles } from '../Card.types';

export const CardTitle = ({ children }: CardTitles) => {
  return (
    <Typography variant="subtitle1" component="span">
      {children}
    </Typography>
  );
};
