import {
  FormControl,
  InputLabel,
  ListSubheader,
  MenuItem,
  Select,
} from '@mui/material';
import { useState } from 'react';

import * as styles from './NavbarActionSelect.styles';
import { NavbarSelectTypes, SelectValue } from './NavbarActionSelect.types';

function assertIsSelectValue(value: any): asserts value is SelectValue {
  const isSelectValue = Object.keys(SelectValue).includes(value);
  if (!isSelectValue) {
    throw new Error('Provided value is not SelectValue');
  }
}

export const NavbarActionSelect = ({
  selectedRows,
  deleteAction,
}: NavbarSelectTypes) => {
  const [selectAction, setSelectAction] = useState<SelectValue>();

  const handleRemove = () => {
    deleteAction(selectedRows);
  };

  return (
    <FormControl sx={styles.actionsSelectContainer}>
      <InputLabel htmlFor="grouped-select">Actions</InputLabel>
      <Select<SelectValue>
        disabled={selectedRows.length === 0}
        value={selectAction}
        id="grouped-select"
        label="Grouping"
        onChange={(e) => {
          assertIsSelectValue(e.target.value);
          setSelectAction(e.target.value);
        }}
      >
        <ListSubheader>Actions</ListSubheader>
        <MenuItem onClick={handleRemove} value={SelectValue.delete}>
          Delete
        </MenuItem>
      </Select>
    </FormControl>
  );
};
