export interface NavbarSelectTypes {
  selectedRows: number[];
  deleteAction: (selectedIds: number[]) => void;
}
export enum SelectValue {
  delete = 'delete',
}
